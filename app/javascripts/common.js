(function($) {
  /*****No javascript*****/
  $('body').removeClass('no-js').addClass('is-js');
  if($('.filter-search').length){
    $('.filter-search .input-group-btn').each(function(){
      $(this).find('select').multiselect({
        includeSelectAllOption: true
      });
    });
  }
  //========================================
  // Fixed header
  //========================================
  function headerTop() {
    var ptopWrapper = $('.header-wrap').outerHeight();
    $('#wrapper').css({ 'padding-top': +ptopWrapper });
  }
  function headerTopReset() {
    $('.leaflet-wrap').css({'left': 0});
    $('#wrapper').css({ 'padding-top': '' });
  }
  //========================================
  //▼ Handle accordion
  //========================================
  var accordionSlider = new $.DEVFUNC.accordion({
    wrap: $('.accordion-menu'),
    item: $('.accordion-item'),
    target: '.accordion-title',
    contents: $('.accordion-content'),
    contentStr: '.accordion-content',
    hasClassSub: 'accordion-content'
  });
  accordionSlider.handleAccordion();
  if($(".table-scroll").length){
    $(".table-scroll").each(function(){
      // ScrollBooster
      var scroll_table = new ScrollBooster({
        viewport: $(this).find('.table-responsive')[0],
        content: $(this).find('table')[0],
        scrollMode: 'native',
        bounceForce: 0.1,
        emulateScroll: true
      });
    });
  }
  //========================================
  //▼ Handle popup
  //========================================
  if ($('.btn-tracking').length && $('.box-paramet').length) {
    $('.btn-tracking').on('click', function(e){
      e.preventDefault();
      if($('.box-paramet').is(':visible')){
        $('.box-paramet').fadeOut(300);
        $(this).removeClass('active');
      }else{
        $('.box-paramet').fadeIn(200);
        $(this).addClass('active');
      }
    });
  }
  //========================================
  //▼ Handle layout
  //========================================
  function handleLayout(){
    var winWidth, contWidth, hgtHeader, hgtWindow, distance, hgtRest;
    winWidth = $(window).width();
    contWidth = $('.container').width();
    hgtHeader = $('#header').innerHeight();
    hgtFront = $('.front-top').innerHeight();
    hgtWindow = $(window).height();
    hgtRest = hgtHeader + hgtFront;
    $('.leaflet-wrap').css({'top': +hgtRest});
    if(winWidth > contWidth){
      distance = (winWidth - contWidth) / 2;
      $('.leaflet-wrap').css({'left': +distance});
    }else{
      $('.leaflet-wrap').css({'left': 0});
    }
  }
  handleLayout();
  //========================================
  //▼ Processing for each breakpoint
  //========================================
  // Processing for each breakpoint
  $.HANDLEBREAKPOINT = function(device) {
    $.DEVFUNC.elemMove($.GSET.MOVE_ELEM, device); //Move element
    console.log(device)
    if(device == 'pc'){
      // Fixed Header
      $(window).on('scroll.fixedHeader', headerTop);
      //==================================================
      //▼ Only processed once when loading the screen end
      //==================================================
      $(window).resize(function() {
        handleLayout();
        headerTop();
      });
    }else{
      $(window).off('scroll.fixedHeader');
      headerTopReset();
      $(window).resize(function() {
        headerTopReset();
      });
    }
  }
  $.DEVFUNC.MATCHMEDIA();
  //==================================================
  //▼ Only processed once when loading the screen end
  //==================================================
  var timer = false;
  $(window).resize(function() {
    if (timer !== false) {
      clearTimeout(timer);
    }
    timer = setTimeout(function() {
      
    }, 100);
  });
})(jQuery);